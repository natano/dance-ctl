/*
 * Copyright (c) 2020 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <Adafruit_STMPE610.h>
#include <EEPROM.h>
#include <HID.h>

static const uint8_t hidReportDescriptor[] PROGMEM = {
    0x05, 0x01,   // USAGE_PAGE (Generic Desktop)
    0x09, 0x05,   // USAGE (Game Pad)
    0xa1, 0x01,   // COLLECTION (Application)
    0xa1, 0x00,   //  COLLECTION (Physical)
    0x85, 0x03,   //    REPORT_ID (3)

    // Buttons
    0x05, 0x09,   //    USAGE_PAGE (Button)
    0x19, 0x01,   //    USAGE_MINIMUM (Button 1)
    0x29, 0x04,   //    USAGE_MAXIMUM (Button 4)
    0x15, 0x00,   //    LOGICAL_MINIMUM (0)
    0x25, 0x01,   //    LOGICAL_MAXIMUM (1)
    0x75, 0x01,   //    REPORT_SIZE (1)
    0x95, 0x04,   //    REPORT_COUNT (4)
    0x81, 0x02,   //    INPUT (Data, Var, Abs)

    // Padding
    0x75, 0x01,   //    REPORT_SIZE (1)
    0x95, 0x04,   //    REPORT_COUNT (4)
    0x01, 0x01,   //    INPUT (Constant)

    0xc0,         //  END_COLLECTION
    0xc0,         // END_COLLECTION
};

#define NSENSORS 4
static struct {
    const char *name;
    const int pin;
    int activateThreshold;
    int deactivateThreshold;
    int val;
} sensors[NSENSORS] = {
    {"LEFT",  A0, 800, 750},
    {"UP",    A1, 550, 500},
    {"RIGHT", A2, 350, 300},
    {"DOWN",  A3, 550, 500},
};

Adafruit_STMPE610 ts = Adafruit_STMPE610(8);
Adafruit_ILI9341 tft = Adafruit_ILI9341(10, 9);

void setup() {    
    pinMode(LED_BUILTIN, OUTPUT);

    static HIDSubDescriptor node(hidReportDescriptor, sizeof(hidReportDescriptor));
    HID().AppendDescriptor(&node);

    tft.begin();

    if (!ts.begin()) {
        Serial.println("Couldn't start touchscreen controller");
        while (1);
    }

    loadThresholds();
}

void loop() {
    static uint8_t sensorStates;
    static uint8_t lastReportData;
    uint8_t reportData;
    bool sendReport;
    int i;

    sendReport = false;

    /*
     * Read the value of all sensors and determine their on/off state depending
     * on the activate/deactivate thresholds.
     */

    for (i = 0; i < NSENSORS; i++) {
        int val = analogRead(sensors[i].pin);
        sensors[i].val = val;

        if (bitRead(sensorStates, i)) {
            if (val < sensors[i].deactivateThreshold) {
                bitClear(sensorStates, i);
                sendReport = true;
            }
        } else {
            if (val >= sensors[i].activateThreshold) {
                bitSet(sensorStates, i);
                sendReport = true;
            }
        }
    }

    /*
     * Report the button states if any of them changed since the last report.
     */
    if (sendReport) {
        HID().SendReport(0x03, &sensorStates, sizeof(sensorStates));
        digitalWrite(LED_BUILTIN, sensorStates ? HIGH : LOW);

        lastReportData = reportData;
    }

    drawMenu();
}

bool getTouch(TS_Point *p) {
    static unsigned long lastClick;
    static bool continuous;
    unsigned long now;

    if (ts.bufferEmpty()) {
        continuous = false;
        return false;
    }

    *p = ts.getPoint();
    delay(50);

    now = millis();
    if (now < lastClick + 500)
        return false;

    // XXX: needs calibration
    p->x = map(p->x, 150, 3800, 0, tft.width());
    p->y = map(p->y, 130, 4000, 0, tft.height());

    if (!continuous) {
        lastClick = millis();
        continuous = true;
    }

    return true;
}

#define SCREEN_ARROWS 1
#define SCREEN_TWEAK_LEFT 2
#define SCREEN_TWEAK_UP 3
#define SCREEN_TWEAK_RIGHT 4
#define SCREEN_TWEAK_DOWN 5

int drawArrows(bool initial) {
    if (initial) {
        tft.fillScreen(ILI9341_BLACK);

        tft.drawRect(0, 80, 80, 80, ILI9341_BLUE);    // left
        tft.fillTriangle(60, 90, 20, 120, 60, 150, ILI9341_BLUE);

        tft.drawRect(80, 0, 80, 80, ILI9341_RED);     // up
        tft.fillTriangle(90, 60, 120, 20, 150, 60, ILI9341_RED);

        tft.drawRect(160, 80, 80, 80, ILI9341_BLUE);  // right
        tft.fillTriangle(180, 90, 220, 120, 180, 150, ILI9341_BLUE);

        tft.drawRect(80, 160, 80, 80, ILI9341_RED);   // down
        tft.fillTriangle(90, 180, 120, 220, 150, 180, ILI9341_RED);

        tft.setCursor(5, 275);
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(2);
        tft.println("JUST HIT THE ARROWS");
    }

    TS_Point p;
    if (getTouch(&p)) {
        if (p.x >= 0 && p.x < 80 && p.y >= 80 && p.y < 160) {
            drawTweak(true, 0);
            return SCREEN_TWEAK_LEFT;
        } else if (p.x >= 80 && p.x < 160 && p.y >= 0 && p.y < 80) {
            drawTweak(true, 1);
            return SCREEN_TWEAK_UP;
        } else if (p.x >= 160 && p.x < 240 && p.y >= 80 && p.y < 160) {
            drawTweak(true, 2);
            return SCREEN_TWEAK_RIGHT;
        } else if (p.x >= 80 && p.x < 160 && p.y >= 160 && p.y < 240) {
            drawTweak(true, 3);
            return SCREEN_TWEAK_DOWN;
        }
    }

    return SCREEN_ARROWS;
}

int log2lin(int val, int low, int high) {
    return map((long)val * (long)val, 0, 1024L * 1024L, low, high);
}

int drawTweak(bool initial, int idx) {
    static int activateThreshold, deactivateThreshold, lastValue;
    bool redraw = false;

    if (initial) {
        tft.fillScreen(ILI9341_BLACK);

        tft.setCursor(85, 0);
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(4);
        tft.println(sensors[idx].name);

        tft.drawRect(210, 0, 30, 30, ILI9341_DARKGREY);
        tft.setCursor(215, 0);
        tft.setTextColor(ILI9341_DARKGREY);
        tft.setTextSize(4);
        tft.println("X");

        tft.setCursor(80, 40);
        tft.setTextColor(ILI9341_GREEN);
        tft.setTextSize(1);
        tft.println("ACTIVATE THRESHOLD");
        tft.drawRect(90, 60, 60, 60, ILI9341_GREEN);
        tft.drawRect(170, 60, 60, 60, ILI9341_GREEN);
        tft.setCursor(107, 73);
        tft.setTextColor(ILI9341_GREEN);
        tft.setTextSize(5);
        tft.println("-");
        tft.setCursor(187, 73);
        tft.setTextColor(ILI9341_GREEN);
        tft.setTextSize(5);
        tft.println("+");

        tft.setCursor(80, 140);
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(1);
        tft.println("DEACTIVATE THRESHOLD");
        tft.drawRect(90, 160, 60, 60, ILI9341_RED);
        tft.drawRect(170, 160, 60, 60, ILI9341_RED);
        tft.setCursor(107, 173);
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(5);
        tft.println("-");
        tft.setCursor(187, 173);
        tft.setTextColor(ILI9341_RED);
        tft.setTextSize(5);
        tft.println("+");

        tft.drawRect(90, 250, 140, 60, ILI9341_WHITE);
        tft.setCursor(103, 262);
        tft.setTextColor(ILI9341_WHITE);
        tft.setTextSize(5);
        tft.println("SAVE");

        activateThreshold = sensors[idx].activateThreshold;
        deactivateThreshold = sensors[idx].deactivateThreshold;
        lastValue = sensors[idx].val;
        redraw = true;
    }

    TS_Point p;
    if (getTouch(&p)) {
        if (p.x >= 210 && p.x < 240 && p.y >= 0 && p.y < 30) {
            drawArrows(true);
            return SCREEN_ARROWS;
        } else if (p.x >= 90 && p.x < 150 && p.y >= 60 && p.y < 120) {
            activateThreshold = max(activateThreshold - 10, 0);
            redraw = true;
        } else if (p.x >= 170 && p.x < 230 && p.y >= 60 && p.y < 120) {
            activateThreshold = min(activateThreshold + 10, 1024);
            redraw = true;
        } else if (p.x >= 90 && p.x < 150 && p.y >= 160 && p.y < 220) {
            deactivateThreshold = max(deactivateThreshold - 10, 0);
            redraw = true;
        } else if (p.x >= 170 && p.x < 230 && p.y >= 160 && p.y < 220) {
            deactivateThreshold = min(deactivateThreshold + 10, 1024);
            redraw = true;
        } else if (p.x >= 90 && p.x < 230 && p.y >= 250 && p.y < 310) {
            sensors[idx].activateThreshold = activateThreshold;
            sensors[idx].deactivateThreshold = deactivateThreshold;
            storeConfig();
            drawArrows(true);
            return SCREEN_ARROWS;
        }
    }

    if (sensors[idx].val < lastValue - 10 || sensors[idx].val > lastValue + 10)
        redraw = true;

    if (redraw) {
        int y;

        tft.fillRect(0, 0, 80, 320, ILI9341_BLACK);

        y = log2lin(activateThreshold, 319, 0);
        tft.drawLine(0, y, 70, y, ILI9341_GREEN);
        tft.setCursor(0, y - 10);
        tft.setTextSize(1);
        tft.setTextColor(ILI9341_GREEN);
        tft.println(activateThreshold);

        y = log2lin(deactivateThreshold, 319, 0);
        tft.drawLine(0, y, 70, y, ILI9341_RED);
        tft.setCursor(0, y + 5);
        tft.setTextSize(1);
        tft.setTextColor(ILI9341_RED);
        tft.println(deactivateThreshold);

        y = log2lin(sensors[idx].val, 319, 0);
        tft.drawLine(0, y, 70, y, ILI9341_WHITE);

        lastValue = sensors[idx].val;
    }

    return SCREEN_TWEAK_LEFT + idx;
}

void drawMenu() {
    static int screen;

    if (screen == 0) {
        screen = SCREEN_ARROWS;
        drawArrows(true);
    }

    switch (screen) {
    case SCREEN_ARROWS:
        screen = drawArrows(false);
        break;
    case SCREEN_TWEAK_LEFT:
        screen = drawTweak(false, 0);
        break;
    case SCREEN_TWEAK_UP:
        screen = drawTweak(false, 1);
        break;
    case SCREEN_TWEAK_RIGHT:
        screen = drawTweak(false, 2);
        break;
    case SCREEN_TWEAK_DOWN:
        screen = drawTweak(false, 3);
        break;
    }
}

void loadThresholds() {
    sensors[0].activateThreshold = constrain((*EEPROM[0] << 8) | *EEPROM[1], 0, 1024);
    sensors[0].deactivateThreshold = constrain((*EEPROM[2] << 8) | *EEPROM[3], 0, 1024);
    sensors[1].activateThreshold = constrain((*EEPROM[4] << 8) | *EEPROM[5], 0, 1024);
    sensors[1].deactivateThreshold = constrain((*EEPROM[6] << 8) | *EEPROM[7], 0, 1024);
    sensors[2].activateThreshold = constrain((*EEPROM[8] << 8) | *EEPROM[9], 0, 1024);
    sensors[2].deactivateThreshold = constrain((*EEPROM[10] << 8) | *EEPROM[11], 0, 1024);
    sensors[3].activateThreshold = constrain((*EEPROM[12] << 8) | *EEPROM[13], 0, 1024);
    sensors[3].deactivateThreshold = constrain((*EEPROM[14] << 8) | *EEPROM[15], 0, 1024);
}

void storeConfig() {
    EEPROM[0].update(sensors[0].activateThreshold >> 8);
    EEPROM[1].update(sensors[0].activateThreshold & 0xff);
    EEPROM[2].update(sensors[0].deactivateThreshold >> 8);
    EEPROM[3].update(sensors[0].deactivateThreshold & 0xff);
    EEPROM[4].update(sensors[1].activateThreshold >> 8);
    EEPROM[5].update(sensors[1].activateThreshold & 0xff);
    EEPROM[6].update(sensors[1].deactivateThreshold >> 8);
    EEPROM[7].update(sensors[1].deactivateThreshold & 0xff);
    EEPROM[8].update(sensors[2].activateThreshold >> 8);
    EEPROM[9].update(sensors[2].activateThreshold & 0xff);
    EEPROM[10].update(sensors[2].deactivateThreshold >> 8);
    EEPROM[11].update(sensors[2].deactivateThreshold & 0xff);
    EEPROM[12].update(sensors[3].activateThreshold >> 8);
    EEPROM[13].update(sensors[3].activateThreshold & 0xff);
    EEPROM[14].update(sensors[3].deactivateThreshold >> 8);
    EEPROM[15].update(sensors[3].deactivateThreshold & 0xff);
}
